#!/usr/bin/python
from gi.repository import Gtk
import webbrowser
import os
import Marcadores

class Buscador(Gtk.Window):

    def __init__(self):
        # Directorio BASE donde esta el SCRIPT
        BASE_DIR = os.path.dirname(__file__)

        builder = Gtk.Builder()
        builder.add_from_file(BASE_DIR + '/resources/Buscador_GUI.glade')


        # Obtener los objetos de la interfaz Glade
        window = builder.get_object('app')

        self.entry = builder.get_object('buscar')
        self.aboutdialogo = builder.get_object('aboutdialog')
        self.btn_marcadores = builder.get_object('btn_marcadores')

        # Senales de los Objetos
        signals = {
            'on_app_delete_event' : Gtk.main_quit,
            'on_menuSalir_activate': self.menuSalir,
            'on_menuEliminar_activate':self.menuEliminar,
            'on_borrarLogs_activate': self.limpiarLogs,
            'on_about_activate': self.about,
            'on_aboutdialog_close':self.dialogdestroy,
            'on_buscar_activate': self.entry_buscar,
            'on_btn_marcadores_clicked': self.btn,  
            }
        builder.connect_signals(signals)

        
        # archivo con las busquedas realizadas
        f = open(BASE_DIR + '.logs', 'r')

        with f:
            self.data = f.readlines()          
        
        
        # Crear el autocompletado 
        autocompletar = Gtk.EntryCompletion()
        self.listado_de_logs = Gtk.ListStore(str)

        # Si hay Logs lo agrega al listado
        for logs in self.data:
            self.listado_de_logs.append([logs])
        
        autocompletar.set_model(self.listado_de_logs)
        
        self.entry.set_completion(autocompletar)
        
        autocompletar.set_text_column(0)

        # Mostar la Aplicacion
        window.show_all() 

        # Variables
        self.browser = webbrowser
        self.BookMark = Marcadores


    # logica principal del Programa
    def entry_buscar(self, widget):
        self.SALIR = Gtk.main_quit()
        
        texto = self.entry.get_text()
        if texto:
            if texto not in [row[0] for row in self.listado_de_logs]:
                ''' Si el texto a buscar no se encuentra en el archivo logs,
                     abre el archivo '.logs' y lo agrega'''

                f = open(BASE_DIR + '.logs', 'a')
                f.write(texto + '\n')
                f.close()

            # busqueda en youtube
            if texto[:2] == 'y:':
                self.browser.open(
                            'https://www.youtube.com/results?search_query='\
                            + texto[2::]
                        )

                self.SALIR

            # busqueda en wikipedia
            elif texto[:2] == 'w:': 
                self.browser.open('http://es.wikipedia.org/wiki/' + texto[2::])

                self.SALIR

            else: 
                ''' Busqueda por Google. |   Por DEFAULT   |

                Esta busqueda se realizara si el usuario no introduce 
                uno de los filtros <|  g: y: w:  |>

                En caso de que el usuario SI introduzca el filtro | g: | se
                hacen los cortes correspondientes y se realiza la busqueda

                '''
                if texto[:2] == 'g:':
                    texto = texto[2::]

                self.browser.open(
                        'https://www.google.com.do/search?safe=off&site\
                        =webhp&source=hp&q=' + texto
                     )
                
                self.SALIR


        # Si no introduce nada y preciona enter
        else: 
            self.SALIR
  

    def menuSalir(self, widget):
        Gtk.main_quit()
 
    def menuEliminar(self, widget):
        self.entry.set_text('')

    def about(self, widget):
        self.aboutdialogo.show()

    def dialogdestroy(self, widget):
        self.aboutdialogo.destroy()

    def limpiarLogs(self, widget):
        f = open(BASE_DIR + '.logs','w')
        f.write('')
        f.close()

    def btn(self, widget):
        return self.BookMark.Marcadores()

    

if __name__ == '__main__':
    Buscador()
    Gtk.main()