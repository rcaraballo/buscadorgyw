#!/usr/bin/env python

from gi.repository import Gtk
import time
import json
import os
import webbrowser

class Marcadores(Gtk.Window):
	def __init__(self):
		# Directorio BASE donde esta el SCRIPT
		BASE_DIR = os.path.dirname(__file__)
		
		# interfaz Principal
		builder = Gtk.Builder()
		builder.add_from_file(BASE_DIR + '/resources/Marcadores_GUI.glade')

		# senales de los objetos : manejadores
		signals = {
			'on_window_marcador_delete_event': Gtk.main_quit,

		}	 
		builder.connect_signals(signals)

		# obtener los objetos de la interfaz Principal 
		window = builder.get_object('window_marcador')
		self.treeview = builder.get_object('treeview')
		self.listado = builder.get_object('liststore')

		# Hacer que se pueda seleccionar el marcador
		select = self.treeview.get_selection()

		# Conectar la variable select a la Funcion controladora
		select.connect('changed', self.seleccion)

		#crear columnas para el treeview
		columna1 = Gtk.TreeViewColumn('Nombre del Marcador')
		columna2 = Gtk.TreeViewColumn('URL')

		# agregando las columnas al treeview
		self.treeview.append_column(columna1)
		self.treeview.append_column(columna2)

		# Renderizar el texto para poder visualizarlo en el Listado
		cell1 = Gtk.CellRendererText()
		columna1.pack_start(cell1, False)
		columna1.add_attribute(cell1, 'text', 0)

		cell2 =Gtk.CellRendererText()
		columna2.pack_start(cell2, False)
		columna2.add_attribute(cell2, 'text', 1)

		# Mostrar la Aplicacion
		window.show_all()

		# Buscar y leer Los Marcadores del Chrome
		Bookmarks_File = os.path.expanduser('~/.config/google-chrome/Default/Bookmarks')

		with open(Bookmarks_File) as f:
			json_data = json.load(f)

		# Obtener todos los marcadores de la barra de marcadores
		buscar_en = json_data['roots']['bookmark_bar']['children']
		
		# Agregando los datos a la variable self.listado
		for children in buscar_en:
			name = children['name'].encode('utf-8')
			if len(name) > 50:
				name = name[:49] + '...'

			self.listado.append([name, children['url']])


	def seleccion(self, select):
		model, treeiter = select.get_selected()
		if treeiter:
			url = model[treeiter][1]

			webbrowser.open(url)

			Gtk.main_quit()


if __name__ == '__main__':
	Marcadores()
	Gtk.main()